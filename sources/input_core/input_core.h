#pragma once

#include "keyboard.h"

#include <memory>

namespace Faber::input
{
	class InputController
	{
	public:
		void init();
		void fetchInput();
	private:
		std::unique_ptr<KeyboardController> keyboard;
	};
}