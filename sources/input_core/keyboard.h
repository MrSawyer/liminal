#pragma once
#include <unordered_map>

namespace Faber::input
{
	enum class KeyboardKey
	{
		F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12,
		Num1, Num2, Num3, Num4, Num5, Num6, Num7, Num8, Num9, Num0,
		Q,W,E,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M,
		LShift, LCtrl, LAlt, Space, RAlt, RCtr, RShift, Enter,
		Numpad0, Numpad1, Numpad2, Numpad3, Numpad4, Numpad5, Numpad6, Numpad7, Numpad8, Numpad9
	};

	class KeyboardController
	{
	private:
		std::unordered_map<KeyboardKey, bool> key_state;
	};
}