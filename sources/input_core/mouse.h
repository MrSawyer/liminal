#pragma once
#include <unordered_map>

namespace Faber::input
{
	enum class MouseKey
	{
		LMB, MMB, RMB
	};

	class MouseController
	{
	public:

	private:
		std::unordered_map<MouseKey, bool> key_state;
		std::size_t mouse_roll_delta;
	};
}