#include "instance.h"

#include <core/logger/logger.h>

namespace Faber::core
{
	bool Instance::isValid() const
	{
		return valid;
	}

	std::string Instance::getInfo() const
	{
		std::stringstream ss;
		ss << "[Instance{";
		ss << GETINFO(valid);
		ss << "}]";
		return ss.str();
	}

	Instance::~Instance()
	{
	}

}