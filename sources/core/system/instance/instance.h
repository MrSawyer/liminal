#pragma once
#include <mutex>

namespace Faber::core
{
	class Instance 
	{
	public:
		Instance() : valid(false) {};

		Instance(const Instance&) = delete;
		void operator=(const Instance&) = delete;
		virtual ~Instance();

		bool isValid() const;

		virtual std::string getInfo() const;
	protected:
		bool valid;
		mutable std::mutex instance_mutex;
	};
}