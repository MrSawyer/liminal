#include "instance_win.h"

#include <core/logger/logger.h>

namespace Faber::core::win {

	InstanceWin::InstanceWin(HINSTANCE instance_)
		: instance(std::move(instance_))
	{
		if (instance_ != nullptr)valid = true;
		else valid = false;
	}

	void InstanceWin::set(HINSTANCE instance_)
	{
		std::lock_guard guard(instance_mutex);

		if (instance_ == nullptr)
		{
			LOG_ERR("Empty instance");
			valid = false;
			return;
		}
		valid = true;
		instance = instance_;
	}

	const HINSTANCE& InstanceWin::get() const
	{
		std::lock_guard guard(instance_mutex);
		return instance;
	}
	std::string InstanceWin::getInfo() const
	{
		std::stringstream ss;
		ss << "[InstanceWin{";
		ss << Instance::getInfo();
		ss << GETINFO(instance);
		ss << "}]";
		return ss.str();
	}
}