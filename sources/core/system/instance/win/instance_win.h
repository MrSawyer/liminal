#pragma once
#include <core/system/instance/instance.h>

#include <Windows.h>

namespace Faber::core::win
{
	class InstanceWin : public Instance
	{
	public:
		InstanceWin() = default;
		InstanceWin(HINSTANCE instance_);

		void set(HINSTANCE instance);
		const HINSTANCE& get() const;

		std::string getInfo() const override;
	private:
		HINSTANCE instance;
	};
}