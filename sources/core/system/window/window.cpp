#include "window.h"

#include <core/logger/logger.h>

namespace Faber::core
{
	Window::Window(Instance* instance_, unsigned int width, unsigned int height)
		: instance(std::move(instance_))
	{
		fullscreen = false;
		onResize(width, height);
	}
	Window::~Window()
	{
	}
	void Window::show()
	{
		visible = true;
	}
	void Window::hide()
	{
		visible = false;
	}
	unsigned int Window::getWidth() const
	{
		return client_width;
	}
	unsigned int Window::getHeight() const
	{
		return client_height;
	}
	float Window::getCursorPositionX()const
	{
		return cursor_positionX;
	}
	float Window::getCursorPositionY()const
	{
		return cursor_positionY;
	}
	bool Window::isFullScreen() const
	{
		return fullscreen;
	}
	std::string Window::getInfo() const
	{
		std::stringstream ss;
		ss << "[Window{";
		ss << GETINFO(max_fps, visible, fullscreen, client_width, client_height, cursor_positionX, cursor_positionY);
		if (instance != nullptr)ss << instance->getInfo();
		ss << "}]";
		return ss.str();
	}
	void Window::setCursourPosition(float x, float y)
	{
		cursor_positionX = x;
		cursor_positionY = y;
	}
	Instance* Window::getInstance() const
	{
		return instance;
	}
	void Window::onResize(unsigned int width, unsigned int height)
	{
		client_width = width;
		client_height = height;
	}
}