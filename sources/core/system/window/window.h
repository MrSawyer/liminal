#pragma once

#include <core/system/instance/instance.h>
#include <core/observer/observer.h>

#include <core/glew/glew.h>
#include <core/glew/wglew.h>

#include <functional>

namespace Faber::core
{
	/**
	* Abstract window class
	*/
	class Window : public virtual ObserverSubject
	{
	public:
		Window(Instance* instance_, unsigned int width, unsigned int height);

		virtual ~Window() = 0;

		virtual void show(); 
		virtual void hide();

		unsigned int getWidth() const;
		unsigned int getHeight() const;

		float getCursorPositionX() const;
		float getCursorPositionY() const;

		bool isFullScreen() const;

		virtual bool good()const = 0;

		virtual std::string getInfo() const; /**< Each class in the program should have a getInfo function 
											 that returns a string describing the state of the class object
											 */

		Instance* getInstance() const; /**< Returns a pointer to the instance of the program that created the window */
		
		virtual void fetchEvents() = 0;
	protected:
		void setCursourPosition(float x, float y);
		virtual void onResize(unsigned int width, unsigned int height);
	private:
		float max_fps;
		Instance * instance;

		bool visible;
		bool fullscreen;

		unsigned int client_width, client_height;
		float cursor_positionX, cursor_positionY;
	};
}