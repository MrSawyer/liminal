#pragma once
#include <core/system/window/window.h>

#include <Windows.h>

namespace Faber::core::win
{
	class WindowWin : public Window
	{
	public:
		WindowWin(Instance* instance, unsigned int width, unsigned int height);
		~WindowWin();

		bool good()const override;
		void show() override;
		void hide() override;

		std::string getInfo() const override;

		HWND getHandle() const;
		void fetchEvents() override;
	private:
		static LRESULT __stdcall g_windowCallback(HWND, UINT, WPARAM, LPARAM);
		LRESULT __stdcall windowCallback(HWND, UINT, WPARAM, LPARAM);


		bool registerClass();
		bool createWindow();

		void onResize(unsigned int width, unsigned int height) override;

		HWND handle;
	};
}