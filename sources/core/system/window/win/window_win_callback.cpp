#include "window_win.h"
#include <core/logger/logger.h>
#include <core/glm/glm/glm.hpp>
#include <core/glm/glm/gtc/matrix_transform.hpp>
#include <windowsx.h>

namespace Faber::core::win
{
	LRESULT __stdcall WindowWin::g_windowCallback(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
	{
		WindowWin* pointer = nullptr;
		if (message == WM_NCCREATE)
		{
			CREATESTRUCT* cs = reinterpret_cast<CREATESTRUCT*>(lparam);
			pointer = reinterpret_cast<WindowWin*>(cs->lpCreateParams);

			SetLastError(0);
			bool success = SetWindowLongPtr(window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pointer)) != 0;
			if (!success && GetLastError() != 0)
			{
				LOG_ERR("Initialization of window class callback failed.");
				PostQuitMessage(1);
				return 0;
			}
			LOG_INF("Window class callback initialized.");
		}
		else
		{
			pointer = reinterpret_cast<WindowWin*>(GetWindowLongPtr(window, GWLP_USERDATA));
		}

		return pointer != nullptr ? pointer->windowCallback(window, message, wparam, lparam) : DefWindowProc(window, message, wparam, lparam);
	}


	LRESULT __stdcall WindowWin::windowCallback(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
	{
		switch (message)
		{
		case WM_MOUSEMOVE:
		{
			setCursourPosition(static_cast<float>(GET_X_LPARAM(lparam)), static_cast<float>(GET_Y_LPARAM(lparam)));
			break;
		}

		case WM_KEYDOWN:
		{
			//input->setKeyboardButtonState((int)wparam, true);
			//input->setKeyCode((int)wparam);
			break;
		}

		case WM_KEYUP:
		{
			//input->setKeyboardButtonState((int)wparam, false);
			break;
		}

		case WM_LBUTTONDOWN:
		{
			//input->setMouseButtonState(0, true);
			break;
		}

		case WM_LBUTTONUP:
		{
			//input->setMouseButtonState(0, false);
			break;
		}

		case WM_RBUTTONDOWN:
		{
			//input->setMouseButtonState(1, true);
			break;
		}

		case WM_RBUTTONUP:
		{
			//input->setMouseButtonState(1, false);
			break;
		}

		case WM_SIZE:
		{
			if (!isFullScreen())
			{
				int width = static_cast<int>(LOWORD(lparam));
				int height = static_cast<int>(HIWORD(lparam));

				if (width > 0 && height > 0)
				{
					onResize(width, height);

					glm::mat4 orthographic_projection = glm::ortho(-(float)getWidth() / 2.0f, (float)getWidth() / 2.0f, -(float)getHeight() / 2.0f, (float)getHeight() / 2.0f);
					glm::mat4 perspective_projection = glm::infinitePerspective(glm::radians(60.0f), (float)getWidth() / (float)getHeight(), 0.1f);
	
				}
			}

			return 0;
		}

		case WM_ERASEBKGND:
		{
			return TRUE;
		}

		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}

		case WM_SYSCOMMAND:
		{
			switch (wparam)
			{
			case SC_SCREENSAVE:
				return 0;

			case SC_MONITORPOWER:
				return 0;
			}
			break;
		}
		}

		return DefWindowProc(window, message, wparam, lparam);
	}


	void WindowWin::fetchEvents()
	{
		MSG msgcontainer = { 0 };
		do{
			if (PeekMessage(&msgcontainer, 0, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msgcontainer);
				DispatchMessage(&msgcontainer);
			}

			switch (msgcontainer.message)
			{
				case WM_QUIT:
				{
					notifyObservers(Event::QUIT_REQUEST); break;
				}
			}

		}while (msgcontainer.message != WM_QUIT);
	}

}