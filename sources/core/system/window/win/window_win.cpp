#include "window_win.h"

#include <core/system/instance/win/instance_win.h>
#include <core/logger/logger.h>

namespace Faber::core::win
{
	WindowWin::WindowWin(Instance* instance, unsigned int width, unsigned int height)
		:Window::Window(std::move(instance), width, height)
	{
		handle = 0;
		registerClass();
		createWindow();
	}

	void WindowWin::show()
	{
		Window::show();
		ShowWindow(handle, SW_SHOW);
		UpdateWindow(handle);
		LOG_INF("Show window command.");
	}

	void WindowWin::hide()
	{
		Window::hide();
		ShowWindow(handle, SW_HIDE);
		LOG_INF("Hide window command.");
	}

	std::string WindowWin::getInfo() const
	{
		std::stringstream ss;
		ss << "[WindowWin{";
		ss << Window::getInfo();
		ss << GETINFO(handle);
		ss << "}]";
		return ss.str();
	}

	HWND WindowWin::getHandle() const
	{
		return handle;
	}

	bool WindowWin::registerClass()
	{
		const InstanceWin* instance = dynamic_cast<const InstanceWin*>(getInstance());
		
		if (instance == nullptr)
		{
			LOG_ERR("Invalid program instance");
			return false;
		}

		WNDCLASS wc = { 0 };
		bool registered = GetClassInfo(instance->get(), "MAIN_WINDOW", &wc);

		if (registered)
		{
			LOG_WAR("Main window class already registered");
			return true;
		}

		wc = { 0 };
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = instance->get();
		wc.hCursor = LoadCursor(0, IDC_ARROW);
		wc.hIcon = 0;
		wc.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1);
		wc.lpfnWndProc = g_windowCallback;
		wc.lpszClassName = "MAIN_WINDOW";
		wc.lpszMenuName = nullptr;
		wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

		return static_cast<bool>(RegisterClass(&wc));
	}

	bool WindowWin::createWindow()
	{
		const InstanceWin* instance = dynamic_cast<const InstanceWin*>(getInstance());
		if (instance == nullptr)
		{
			LOG_ERR("Invalid program instance");
			return false;
		}

		if (isFullScreen())
		{
			int width = GetSystemMetrics(SM_CXSCREEN);
			int height = GetSystemMetrics(SM_CYSCREEN);

			handle = CreateWindowEx(WS_EX_APPWINDOW, "MAIN_WINDOW", "MAIN_WINDOW", WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
				0, 0, width, height, 0, 0, instance->get(), reinterpret_cast<LPVOID>(this));

			onResize(width, height);
		}
		else
		{
			handle = CreateWindowEx(WS_EX_APPWINDOW | WS_EX_WINDOWEDGE, "MAIN_WINDOW", "MAIN_WINDOW",
				WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, CW_USEDEFAULT, CW_USEDEFAULT,
				getWidth(), getHeight(), 0, 0,
				instance->get(), reinterpret_cast<LPVOID>(this));
		}

		show();
		return good();
	}

	void WindowWin::onResize(unsigned int width, unsigned int height)
	{
		Window::onResize(width, height);

		glViewport(0, 0, width, height);
	}

	WindowWin::~WindowWin()
	{
		if (good())
		{
			DestroyWindow(handle);
			handle = 0;
			LOG_INF("Window destroyed.");
		}
	}

	bool WindowWin::good() const
	{
		return handle != 0;
	}
}