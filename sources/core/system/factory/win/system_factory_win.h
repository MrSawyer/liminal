#pragma once

#include <core/system/factory/system_factory.h>

namespace Faber::core::win
{
	class SystemFactoryWin : public SystemFactory
	{
	public:
		SystemFactoryWin() = default;
		~SystemFactoryWin() = default;

		std::unique_ptr<Window> createWindow(Instance* instance, unsigned int width, unsigned int height) const override;
		std::unique_ptr<Renderer> createOpenGLRenderer(Window* window, unsigned int major_version ,unsigned int minor_version)const override;
		std::unique_ptr<Renderer> createDirectXRenderer(Window* window, unsigned int version)const override;

	};
}