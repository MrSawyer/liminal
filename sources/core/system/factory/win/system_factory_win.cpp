#include "system_factory_win.h"
#include <core/logger/logger.h>

#include <core/system/window/win/window_win.h>
#include <core/renderer/opengl/win/opengl_renderer_win.h>
#include <core/renderer/dx/win/directx_renderer_win.h>

namespace Faber::core::win
{
	std::unique_ptr<Window> SystemFactoryWin::createWindow(Instance* instance, unsigned int width, unsigned int height) const
	{
		return std::make_unique<WindowWin>(std::move(instance), width, height);
	}

	std::unique_ptr<Renderer> SystemFactoryWin::createOpenGLRenderer(Window* window, unsigned int major_version, unsigned int minor_version)const
	{
		WindowWin* window_win = dynamic_cast<WindowWin*>(window);

		if (window_win == nullptr)
		{
			LOG_ERR("Passed window is not proper Windows window.");
			return std::make_unique<OpenGLRendererWin>(nullptr, 0, 0, (HWND)NULL);
		}
		return std::make_unique<OpenGLRendererWin>(std::move(window_win->getInstance()), major_version, minor_version, std::move(window_win->getHandle()));
	}

	std::unique_ptr<Renderer> SystemFactoryWin::createDirectXRenderer(Window* window, unsigned int version)const
	{
		WindowWin* window_win = dynamic_cast<WindowWin*>(window);

		if (window_win == nullptr)
		{
			LOG_ERR("Passed window is not proper Windows window.");
			return std::make_unique<DirectXRendererWin>(nullptr, 0, (HWND)NULL);
		}
		return std::make_unique<DirectXRendererWin>(std::move(window_win->getInstance()), version, std::move(window_win->getHandle()));
	}
}