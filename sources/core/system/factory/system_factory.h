#pragma once
#include <core/system/window/window.h>
#include <core/renderer/renderer.h>
#include <memory>

namespace Faber::core
{
	class SystemFactory
	{
	public:
		SystemFactory() = default;
		virtual ~SystemFactory() = default;

		virtual std::unique_ptr<Window> createWindow(Instance * instance, unsigned int width, unsigned int height) const = 0;
		virtual std::unique_ptr<Renderer> createOpenGLRenderer(Window* window, unsigned int major_version, unsigned int minor_version)const = 0;
		virtual std::unique_ptr<Renderer> createDirectXRenderer(Window* window, unsigned int version)const = 0;
	};
}