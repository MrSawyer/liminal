#include "basic_timer.h"

namespace Faber::core
{
	BasicTimer::BasicTimer()
	{
		stopped = false;
		elapsed_time = std::chrono::milliseconds(0);
	}
	void BasicTimer::start()
	{
		stopped = false;
		timestamp_start = std::chrono::high_resolution_clock::now();
	}
	void BasicTimer::stop()
	{
		if (stopped == false) {
			elapsed_time += std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - timestamp_start);
			stopped = true;
		}
	}
	void BasicTimer::reset()
	{
		stopped = true;
		elapsed_time = std::chrono::milliseconds(0);
	}

	std::chrono::milliseconds BasicTimer::getMiliseconds()
	{
		if (stopped)
		{
			return elapsed_time;
		}
		else
		{
			return elapsed_time + std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - timestamp_start);
		}		
	}
}