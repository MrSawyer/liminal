#pragma once
#include <time.h>
#include <chrono>

namespace Faber::core
{
	class BasicTimer
	{
	public:
		BasicTimer();
		void start();
		void stop();
		void reset();
		std::chrono::milliseconds getMiliseconds();
	private:
		bool stopped;

		std::chrono::steady_clock::time_point timestamp_start;
		std::chrono::duration<long long, std::milli> elapsed_time;
	};
}