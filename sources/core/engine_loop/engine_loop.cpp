#include "engine_loop.h"
#include <core/console/console.h>
#include <core/logger/logger.h>

#include <string>
#include <cstdlib>
#include <time.h>

namespace Faber::core
{
	void EngineLoop::setRenderer(std::unique_ptr<Renderer> rnd)
	{
		renderer = std::move(rnd);
	}

	void EngineLoop::setWindow(std::unique_ptr<Window> wnd)
	{
		window = std::move(wnd);
	}

	Renderer* EngineLoop::getRenderer() const
	{
		return renderer.get();
	}

	Window* EngineLoop::getWindow() const
	{
		return window.get();
	}

	void EngineLoop::preInit(const std::string& command_srguments)
	{
		LOG_INF("EngineLoop preInit");

		srand((unsigned int)time(0));
		Faber::core::createConsole();
		engine_instance = std::make_unique<engine::Engine>();
	}

	void EngineLoop::init()
	{
		LOG_INF("EngineLoop init");

		assets_manager = std::make_unique<engine::AssetsManager>();
		LOG_INF(assets_manager->getInfo());
		assets_manager->loadMesh("data/models/room/.OBJ/Room #1.obj");

		engine_instance->parseArguments();
		engine_instance->init();
		engine_instance->start();
	}

	void EngineLoop::tick()
	{

	}

	void EngineLoop::exit()
	{
		LOG_INF("EngineLoop exit");

		engine_instance->close();
		renderer.reset();
		window.reset();
	}

	void EngineLoop::postExit()
	{
		LOG_INF("EngineLoop postExit");
		Faber::core::deleteConsole();
	}
}