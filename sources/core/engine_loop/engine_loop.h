#pragma once
#include <engine/engine.h>
#include <core/system/window/window.h>
#include <engine/assets/assets_manager.h>
#include <core/renderer/renderer.h>
#include <core/observer/observer.h>

#include <string>
#include <memory>

namespace Faber::core
{
	class EngineLoop : public virtual Observer
	{
	public:
		void preInit(const std::string & command_srguments = "");
		void init();
		void tick();
		void exit();
		void postExit();

		void setRenderer(std::unique_ptr<Renderer> rnd);
		void setWindow(std::unique_ptr<Window> wnd);

		Renderer* getRenderer() const;
		Window* getWindow() const;
	protected:



	private:
		std::unique_ptr<engine::Engine> engine_instance;
		std::unique_ptr<Window> window;
		std::unique_ptr<Renderer> renderer;
		std::unique_ptr<engine::AssetsManager> assets_manager;
	};

}