#include <core/launch/launch.h>

#ifdef _WIN32
#include <core/system/factory/win/system_factory_win.h>
#include <core/system/instance/win/instance_win.h>

int __stdcall WinMain(_In_ HINSTANCE instance, _In_opt_ HINSTANCE previnstance, _In_ LPSTR cmdline, _In_ int cmdshow)
{
	std::shared_ptr<Faber::core::SystemFactory> factory = std::make_shared<Faber::core::win::SystemFactoryWin>();	
	std::unique_ptr<Faber::core::Instance> program_instance = std::make_unique<Faber::core::win::InstanceWin>(instance);

	Faber::core::launchEngine(std::move(program_instance), std::move(factory));

	return 0;
}

#endif