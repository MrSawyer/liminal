#include "launch.h"
#include <core/engine_loop/engine_loop.h>
#include <core/logger/logger.h>

namespace Faber::core {
	void launchEngine(
		std::unique_ptr<Faber::core::Instance> instance,
		std::shared_ptr<Faber::core::SystemFactory> system_factory)
	{
		EngineLoop engine_loop;
		
		engine_loop.preInit();

		engine_loop.setWindow(system_factory->createWindow(instance.get(), 1200, 900));
		engine_loop.setRenderer(system_factory->createOpenGLRenderer(engine_loop.getWindow(), 3, 3));

		engine_loop.getWindow()->attachObserver(&engine_loop);

		engine_loop.init();

		while (engine_loop.fetchEventBack().event != Event::QUIT_REQUEST) 
		{
			engine_loop.getWindow()->fetchEvents();
			engine_loop.tick();
		}

		engine_loop.exit();

		std::cin.get();
		engine_loop.postExit();
	}
}