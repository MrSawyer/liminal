#pragma once

#include <core/system/instance/instance.h>
#include <core/system/factory/system_factory.h>

namespace Faber::core {
	/**
	* 
	*/
	void launchEngine(std::unique_ptr<Faber::core::Instance> instance, std::shared_ptr<Faber::core::SystemFactory> system_factory);
}