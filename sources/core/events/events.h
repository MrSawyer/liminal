#pragma once
#include <string>

namespace Faber::core
{
	enum class Event : unsigned int
	{
		NOP, QUIT_REQUEST
	};

	std::string stringifyEnum(const Event & e);
}