#include "events.h"

namespace Faber::core
{
	std::string stringifyEnum(const Event& e)
	{
		switch (e)
		{
		case Event::NOP: return "NOP";
		case Event::QUIT_REQUEST: return "QUIT_REQUEST";
		}

		return "";
	}
}