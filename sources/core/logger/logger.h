#pragma once

#include <core/macros/streamize.h>

#include <iostream>
#include <string>
#include <fstream>
#include <filesystem>
#include <Windows.h>
#include <sstream>

#define FABER_LOG_ENABLE 1

namespace Faber::core
{
#if FABER_LOG_ENABLE

	const std::string LOG_FILE_PATH = "log";
	const std::string LOG_FILE_EXTENSION = "log";
	const std::string PROJECT_CODE_DIR = "sources";
	const std::string DIRECTORY_SEPARATOR = "\\";

	class Logger {
	public:
		Logger(Logger& other) = delete;
		Logger(const Logger&) = delete;
		Logger(Logger&&) = delete;
		Logger& operator=(const Logger&) = delete;
		Logger& operator=(Logger&&) = delete;

		~Logger();

		static Logger& get();

		void writeLogToFile(const std::stringstream& msg);

	private:
		Logger();

		std::fstream file;
	};

	void printError(const std::stringstream& msg, unsigned int color);


#define GETINFO(...) std::string{" "} << EXPAND(STREAMIZE(__VA_ARGS__)) << ""

#define MAKE_LOG(lv, msg, color)\
	do {\
		std::stringstream log; \
		std::string relative_path(__FILE__); \
		if(relative_path.find(Faber::core::PROJECT_CODE_DIR) != std::string::npos) relative_path = relative_path.substr(relative_path.find(Faber::core::PROJECT_CODE_DIR)); \
		log << lv << "[" << relative_path << "][" << __func__ << "()](" << std::to_string(__LINE__) << ") " << msg << "\n"; \
		Faber::core::Logger::get().writeLogToFile(log); \
		Faber::core::printError(log, color); \
	}while(0);\

#define LOG_ASSERT_MSG(condition, msg)\
	do { \
		if (condition) break; \
		MAKE_LOG("ASSERT", "<"<< #condition <<" : False> "<<msg, 12);	\
		Faber::core::Logger::get().~Logger(); \
		std::abort(); \
	} while (0); \

#define LOG_ASSERT(condition)LOG_ASSERT_MSG(condition, "")

#define LOG_ERR(msg) MAKE_LOG("ERR", msg, 12);

#define LOG_WAR(msg) MAKE_LOG("WAR", msg, 14);

#define LOG_INF(msg) MAKE_LOG("INF", msg, 10);


#else
#define MAKE_LOG(...)
#define LOG_ASSERT_MSG(...)
#define LOG_ASSERT(...)
#define LOG_ERR(...)
#define LOG_WAR(...)
#define LOG_INF(...)
#endif

	std::string GetLastErrorAsString();

}