#include "logger.h"

#include <filesystem>
#include <time.h>

namespace Faber::core
{
#if FABER_LOG_ENABLE
	void Logger::writeLogToFile(const std::stringstream& msg)
	{

		if (file.good())file << __TIME__ << " " << __DATE__ << " | " << msg.str();
		else
		{
			std::stringstream ss;
			ss << "Cannot write to log file\n";
			ss << " good()=" << file.good();
			ss << " eof()=" << file.eof();
			ss << " fail()=" << file.fail();
			ss << " bad()=" << file.bad();
			printError(ss, 12);
		}
	}

	Logger::Logger()
	{
		__time64_t long_time;
		_time64(&long_time);

		struct tm ltm;
		errno_t err = _localtime64_s(&ltm, &long_time);
		
		if (err)
		{
			std::cout << "Invalid argument to _localtime64_s. Error:" << err << std::endl;
			return;
		}

		std::string relative_path(__FILE__); \
			if (relative_path.find("liminal") != std::string::npos) relative_path = relative_path.substr(0, relative_path.find("liminal"));

		std::string date = std::to_string(ltm.tm_hour) + "-" + std::to_string(ltm.tm_min) + "-" + std::to_string(ltm.tm_hour)
			+ "_" + std::to_string(ltm.tm_mday) + "-" + std::to_string(ltm.tm_mon + 1) + "-" + std::to_string(ltm.tm_year + 1900);

		std::string log_path = relative_path + "liminal" + DIRECTORY_SEPARATOR + "logs" + DIRECTORY_SEPARATOR + LOG_FILE_PATH + "_" + date + "." + LOG_FILE_EXTENSION;

		file.open(log_path, std::fstream::out | std::fstream::app);
		std::cout << "Creating log file : " << log_path << std::endl;
	}

	Logger::~Logger()
	{
		file.close();
	}

	Logger& Logger::get()
	{
		static Logger instance_;
		return instance_;
	}

#ifdef _WIN32
	void printError(const std::stringstream& log, unsigned int color)
	{
		static CONSOLE_SCREEN_BUFFER_INFO Info;
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &Info);
		static WORD Attributes = Info.wAttributes;

		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (WORD)color);
		std::cerr << log.rdbuf();
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Attributes);
	}
#else
	void printError(const std::stringstream& log, unsigned int color)
	{
		// TUTAJ BED� LINUXOWE RZECZY ALE JESZCZE NIE WIEM JAK TO TAM DZIA�A
	}
#endif // OS

#endif // LOG_ENABLE

	std::string GetLastErrorAsString()
	{
		DWORD errorMessageID = ::GetLastError();
		if (errorMessageID == 0) {
			return std::string();
		}

		LPSTR messageBuffer = nullptr;

		size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

		std::string message(messageBuffer, size);

		LocalFree(messageBuffer);

		return message;
	}

}
