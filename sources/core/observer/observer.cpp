#include "observer.h"
#include <core/logger/logger.h>

namespace Faber::core
{
    const std::size_t MAX_OBSERVER_QUEUE_SIZE = 256;

    std::string ObserverMessage::getInfo() const
    {
        std::string event_desc = stringifyEnum(event);
        std::stringstream ss;
        ss << "[ObserverMessage{";
        ss << GETINFO(event_desc, params);
        ss << "}]";
        return ss.str();
    }

    std::string Observer::getInfo() const
    {
        std::stringstream ss;
        ss << "[Observer{";
        ss << GETINFO(number_of_subscriptions, msg_queue.size());
        ss << "}]";
        return ss.str();
    }

    void Observer::observerMessageReceived(const ObserverMessage& msg)
    {
        LOG_INF("Obtained message : " << msg.getInfo());

        while (msg_queue.size() >= MAX_OBSERVER_QUEUE_SIZE)
        {
            msg_queue.pop_back();
        }

        msg_queue.push_front(msg);
    }

    std::size_t Observer::getNumberOfSubscribers() const
    {
        return number_of_subscriptions;
    }

    std::size_t Observer::getMessageQueueSize() const
    {
        return msg_queue.size();
    }

    ObserverMessage Observer::popEventBack()
    {
        if (msg_queue.size() == 0)
        {
            return ObserverMessage(Event::NOP);
        }

        ObserverMessage msg = fetchEventBack();
        msg_queue.pop_back();
        return msg;
    }

    ObserverMessage Observer::fetchEventBack() const
    {
        if (msg_queue.size() == 0)
        {
            return ObserverMessage(Event::NOP);
        }
        return  msg_queue.back();
    }

    void Observer::clear()
    {
        msg_queue.clear();
    }

    std::string ObserverSubject::getInfo() const
    {
        std::stringstream ss;
        ss << "[ObserverSubject{";
        ss << GETINFO(list_observer.size());
        ss << "}]";
        return ss.str();
    }

    void ObserverSubject::attachObserver(Observer* observer) {
        if (observer == nullptr)
        {
            LOG_WAR("Attaching subject to empty observer");
            return;
        }

        observer->number_of_subscriptions++;
        list_observer.push_back(observer);
    }

    void ObserverSubject::detachObserver(Observer* observer) {
        if (observer == nullptr)
        {
            LOG_WAR("Detaching subject from empty observer");
            return;
        }
        observer->number_of_subscriptions++;
        list_observer.remove(observer);
    }

    void ObserverSubject::notifyObservers(const ObserverMessage& msg){
        std::list<Observer*>::iterator iterator = list_observer.begin();
        while (iterator != list_observer.end()) 
        {

            if (*iterator == nullptr)
            {
                LOG_WAR("There is an empty observer on the list of subscribers.");
            }
            else
            {
               (*iterator)->observerMessageReceived(msg);
            }

            ++iterator;
        }
    }

    void ObserverSubject::notifyObservers(const Event& event)
    {
        ObserverMessage msg;
        msg.event = event;
        msg.params = "";

        notifyObservers(msg);
    }
}