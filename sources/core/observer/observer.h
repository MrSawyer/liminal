#pragma once

#include <core/events/events.h>
#include <list>
#include <deque>
#include <string>

namespace Faber::core
{
	struct ObserverMessage
	{
		ObserverMessage() = default;
		ObserverMessage(Event e, std::string args) : event(std::move(e)), params(std::move(args)) {}
		ObserverMessage(Event e) : event(std::move(e)) {}

		Event event;
		std::string params;

		std::string getInfo() const;
	};

	class Observer {
	public:
		Observer() : number_of_subscriptions(0) {}
		virtual ~Observer() = default;
		virtual void observerMessageReceived(const ObserverMessage & msg);

		std::size_t getNumberOfSubscribers() const;
		std::size_t getMessageQueueSize() const;

		ObserverMessage popEventBack();
		ObserverMessage fetchEventBack() const;

		void clear();

		std::string getInfo() const;
	private:
		friend class ObserverSubject;
		std::size_t number_of_subscriptions;
		std::deque<ObserverMessage> msg_queue;

	};

	class ObserverSubject {
	public:

		std::string getInfo() const;

		virtual ~ObserverSubject() = default;
		void attachObserver(Observer* observer);
		void detachObserver(Observer* observer);
		void notifyObservers(const ObserverMessage & msg);
		void notifyObservers(const Event & event);

	private:
		std::list<Observer*> list_observer;
	};
}