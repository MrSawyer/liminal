#include "renderer.h"

namespace Faber::core
{
	std::string Renderer::getInfo() const
	{
		std::stringstream ss;
		ss << "[Renderer{";
		if (instance != nullptr)ss << instance->getInfo();
		ss << "}]";
		return ss.str();
	}
}