#pragma once

#include "buffer_mask.h"
#include <core/system/instance/instance.h>
#include <core/glm/glm/glm.hpp>

namespace Faber::core
{
	class Renderer
	{
	public:
		Renderer(Instance* instance_) : instance(std::move(instance_)) {}
		virtual ~Renderer() = default;
		virtual void clearBuffers(BufferMask masks) = 0;
		virtual void swapBuffers() = 0;
		virtual void enableDepthTest(bool enable) = 0;
		virtual void enableStencilTest(bool enable) = 0;
		virtual void setBackgroundColor(glm::vec4 color) = 0;
		virtual void setDepthClearValue(double depth) = 0;
		virtual void setStencilClearValue(int stencil) = 0;
		virtual void setAsRenderTarget() = 0;
		virtual bool isGood() const = 0;
		virtual bool makeCurrent() const = 0;

		virtual std::string getInfo() const;

	protected:
		Instance* instance;
		mutable std::mutex rnd_mutex;
	};
}