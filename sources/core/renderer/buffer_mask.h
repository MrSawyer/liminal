#pragma once

namespace Faber::core
{
	enum class BufferMask : unsigned int
	{
		COLOR_BUFFER,
		DEPTH_BUFFER,
		STENCIL_BUFFER
	};
}