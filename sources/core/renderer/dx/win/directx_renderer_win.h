#pragma once
#include <core/renderer/dx/directx_renderer.h>
#include <windows.h>


namespace Faber::core::win {
	class DirectXRendererWin : public DirectXRenderer
	{
	public:
		DirectXRendererWin(Instance* instance, unsigned int version, HWND owner);
		DirectXRendererWin(const DirectXRendererWin& other) = delete;
		virtual ~DirectXRendererWin() = default;

		std::string getInfo() const override;
	protected:
	private:
	};
}