#include "directx_renderer_win.h"
#include <core/logger/logger.h>

namespace Faber::core::win
{
	DirectXRendererWin::DirectXRendererWin(Instance* instance, unsigned int version, HWND owner)
		:DirectXRenderer::DirectXRenderer(std::move(instance), version)
	{

	}

	std::string DirectXRendererWin::getInfo() const
	{
		std::stringstream ss;
		ss << "[DirectXRendererWin{";
		ss << DirectXRenderer::getInfo();
		//ss << GETINFO(handle, dev, ctx);
		ss << "}]";
		return ss.str();
	}
}