#include "directx_renderer.h"
#include <core/logger/logger.h>

namespace Faber::core
{
	std::string DirectXRenderer::getInfo() const
	{
		std::stringstream ss;
		ss << "[DirectXRenderer{";
		ss << Renderer::getInfo();
		ss << GETINFO(version);
		ss << "}]";
		return ss.str();
	}

}