#pragma once
#include <core/renderer/renderer.h>

namespace Faber::core {
	class DirectXRenderer : public Renderer
	{
	public:
		DirectXRenderer(Instance* instance_, unsigned int version_) : Renderer::Renderer(std::move(instance_)), version(version_) {};
		virtual ~DirectXRenderer() = default;

		std::string getInfo() const override;

		void clearBuffers(BufferMask masks) {};
		void swapBuffers() {};
		void enableDepthTest(bool enable) {};
		void enableStencilTest(bool enable) {};
		void setBackgroundColor(glm::vec4 color) {};
		void setDepthClearValue(double depth) {};
		void setStencilClearValue(int stencil) {};
		void setAsRenderTarget() {};
		bool isGood() const { return false; };
		bool makeCurrent() const { return false; };

	private:
		unsigned int version;
	};
}