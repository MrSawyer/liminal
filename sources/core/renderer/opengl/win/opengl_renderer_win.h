#pragma once
#include <core/renderer/opengl/opengl_renderer.h>
#include <windows.h>
#include <string>

namespace Faber::core::win {
	class OpenGLRendererWin : public OpenGLRenderer
	{
	public:
		OpenGLRendererWin(Instance * instance, unsigned int major_version, unsigned int minor_version, HWND owner);
		OpenGLRendererWin(const OpenGLRendererWin& other) = delete;
		virtual ~OpenGLRendererWin();

		std::string getInfo() const override;

		void swapBuffers() override;

		bool isGood() const override;
		bool makeCurrent() const override;

	protected:
		PIXELFORMATDESCRIPTOR generatePixelFormatDescriptor() const;

		std::string	registerTemporaryClass();
		HWND		createTemporaryWindow(std::string classname);
		void		deleteTemporaryObjects(std::string classname, HWND handleTMP = 0, HDC devTMP = 0, HGLRC ctxTMP = 0);

		bool getAccessToOpenGLExtensions() override;
		bool createOpenGLContext(unsigned int major_version, unsigned int minor_version) override;
		void deleteOpenGLContext() override;

	private:
		HWND	handle;
		HDC		dev;
		HGLRC	ctx;
	};
}
