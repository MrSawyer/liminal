
#include "opengl_renderer_win.h"
#include <core/system/instance/win/instance_win.h>
#include <core/logger/logger.h>
#include <core/glew/glew.h>
#include <core/glew/wglew.h>


namespace Faber::core::win
{

	std::string OpenGLRendererWin::getInfo() const
	{
		std::stringstream ss;
		ss << "[OpenGLRendererWin{";
		ss << OpenGLRenderer::getInfo();
		ss << GETINFO(handle, dev, ctx);
		ss << "}]";
		return ss.str();
	}

	PIXELFORMATDESCRIPTOR OpenGLRendererWin::generatePixelFormatDescriptor() const
	{
		PIXELFORMATDESCRIPTOR pfd = { 0 };
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.cColorBits = 24;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 24;
		pfd.cStencilBits = 8;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.iLayerType = PFD_MAIN_PLANE;
		LOG_INF("Pixel format descriptor generated.");
		return pfd;
	}

	std::string OpenGLRendererWin::registerTemporaryClass()
	{
		const InstanceWin* instance_win = dynamic_cast<InstanceWin*>(instance);
		if (instance_win == nullptr)
		{
			LOG_ERR("Instance of program is not valid Windows process.");
			return std::string("");
		}

		WNDCLASS wc = { 0 };
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = instance_win->get();
		wc.hCursor = LoadCursor(0, IDC_ARROW);
		wc.hIcon = LoadIcon(0, IDI_APPLICATION);
		wc.hbrBackground = reinterpret_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
		wc.lpfnWndProc = DefWindowProc;
		wc.lpszClassName = "ExterioresTMP";
		wc.lpszMenuName = nullptr;
		wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

		if (!RegisterClass(&wc))
		{
			LOG_ERR("Registering temporary window class failed.");
			return std::string("");
		}

		LOG_INF("Temporary window class registered.");
		return std::string("ExterioresTMP");
	}

	HWND OpenGLRendererWin::createTemporaryWindow(std::string classname)
	{
		const InstanceWin* instance_win = dynamic_cast<InstanceWin*>(instance);
		if (instance_win == nullptr)
		{
			LOG_ERR("Instance of program is not valid Windows process.");
			return 0;
		}

		HWND handleTMP = CreateWindow(classname.c_str(), "", WS_POPUP, 0, 0, 300, 300, 0, 0, instance_win->get(), nullptr);

		if (handleTMP == 0)
		{
			LOG_ERR("Creating temporary window failed.");
			return 0;
		}

		LOG_INF("Temporary window created.");
		return handleTMP;
	}

	void OpenGLRendererWin::deleteTemporaryObjects(std::string classname, HWND handleTMP, HDC devTMP, HGLRC ctxTMP)
	{
		if (ctxTMP != 0)
		{
			wglMakeCurrent(0, 0);
			wglDeleteContext(ctxTMP);
		}
		if (devTMP != 0)
		{
			ReleaseDC(handleTMP, devTMP);
		}
		if (handleTMP != 0)
		{
			DestroyWindow(handleTMP);
		}
		if (!classname.empty())
		{

			const InstanceWin* instance_win = dynamic_cast<InstanceWin*>(instance);
			if (instance_win == nullptr)
			{
				LOG_ERR("Instance of program is not valid Windows process.");
				return;
			}

			UnregisterClass(classname.c_str(), instance_win->get());
		}

		LOG_INF("Temporary objects deleted.");
	}

	bool OpenGLRendererWin::getAccessToOpenGLExtensions()
	{
		std::string classname = registerTemporaryClass();
		if (classname.empty())
		{
			deleteTemporaryObjects(classname);
			return false;
		}

		HWND handleTMP = createTemporaryWindow(classname);
		if (handleTMP == 0)
		{
			deleteTemporaryObjects(classname, handleTMP);
			return false;
		}

		PIXELFORMATDESCRIPTOR pfd = generatePixelFormatDescriptor();
		HDC devTMP = GetDC(handleTMP);

		int pf = ChoosePixelFormat(devTMP, &pfd);
		if (!SetPixelFormat(devTMP, pf, &pfd))
		{
			LOG_ERR("Setting temporary pixel format failed.");
			deleteTemporaryObjects(classname, handleTMP, devTMP);
			return false;
		}
		LOG_INF("Temporary pixel format set.");

		HGLRC ctxTMP = wglCreateContext(devTMP);
		if (ctxTMP == 0)
		{
			LOG_ERR("Creating temporary OpenGL context failed.");
			deleteTemporaryObjects(classname, handleTMP, devTMP, ctxTMP);
			return false;
		}
		LOG_INF("Temporary OpenGL context created.");

		if (!wglMakeCurrent(devTMP, ctxTMP))
		{
			LOG_ERR("Making temporary OpenGL context current failed.");
			deleteTemporaryObjects(classname, handleTMP, devTMP, ctxTMP);
			return false;
		}
		LOG_INF("Temporary OpenGL context current.");

		if (!loadOpenGLExtensions())
		{
			deleteTemporaryObjects(classname, handleTMP, devTMP, ctxTMP);
			return false;
		}

		deleteTemporaryObjects(classname, handleTMP, devTMP, ctxTMP);

		return true;
	}

	bool OpenGLRendererWin::createOpenGLContext(unsigned int major_version, unsigned int minor_version)
	{
		if (!getAccessToOpenGLExtensions())
		{
			PostQuitMessage(2);
			return false;
		}

		PIXELFORMATDESCRIPTOR	pfd = generatePixelFormatDescriptor();
		int						pf = 0;
		unsigned int			pfc = 0;

		const int dev_attributes[] =
		{
			WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
			WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
			WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
			WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
			WGL_COLOR_BITS_ARB, 24,
			WGL_ALPHA_BITS_ARB, 8,
			WGL_DEPTH_BITS_ARB, 24,
			WGL_STENCIL_BITS_ARB, 8,
			WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
			WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
			WGL_SAMPLES_ARB, 8,
			0
		};

		const int ctx_attributes[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, (const int)major_version,
			WGL_CONTEXT_MINOR_VERSION_ARB, (const int)minor_version,
			WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
			0
		};

		if (!wglChoosePixelFormatARB(dev, dev_attributes, nullptr, 1, &pf, &pfc))
		{
			LOG_ERR("Choosing pixel format failed.");
			PostQuitMessage(3);
			return false;
		}

		if (!SetPixelFormat(dev, pf, &pfd))
		{
			LOG_ERR("Setting pixel format failed.");
			PostQuitMessage(3);
			return false;
		}
		LOG_INF("Pixel format set.");

		ctx = wglCreateContextAttribsARB(dev, 0, ctx_attributes);
		if (ctx == 0)
		{
			LOG_ERR("OpenGL " << major_version<<"."<< minor_version <<" not supported.");
			MessageBox(handle, (LPCSTR)("OpenGL " + std::to_string(major_version) + "." + std::to_string(minor_version) + " not supported by your graphics card.").c_str(), "Critical Error", MB_ICONERROR | MB_OK);
			PostQuitMessage(3);
			return false;
		}
		LOG_INF("OpenGL " << major_version << "." << minor_version << " context created.");

		if (!wglMakeCurrent(dev, ctx))
		{
			LOG_ERR("Making OpenGL context current failed.");
			PostQuitMessage(3);
			return false;
		}
		LOG_INF("OpenGL context current.");

		return true;
	}

	void OpenGLRendererWin::deleteOpenGLContext()
	{
		if (ctx != 0)
		{
			wglMakeCurrent(0, 0);
			wglDeleteContext(ctx);
			ctx = 0;
		}
		if (dev != 0)
		{
			ReleaseDC(handle, dev);
			dev = 0;
		}
		LOG_INF("OpenGL context deleted.");
	}

	OpenGLRendererWin::OpenGLRendererWin(Instance* instance, unsigned int major_version, unsigned int minor_version, HWND owner)
		: OpenGLRenderer::OpenGLRenderer(instance, major_version,  minor_version)
	{
		LOG_ASSERT_MSG(owner != 0, "OpenGLSurface owner cannot be null.");

		handle = owner;
		dev = GetDC(handle);
		ctx = 0;

		if (!createOpenGLContext(major_version, minor_version)) deleteOpenGLContext();
		else setOpenGLSettings();
	}

	OpenGLRendererWin::~OpenGLRendererWin()
	{
		deleteOpenGLContext();
	}

	bool OpenGLRendererWin::isGood() const
	{
		return (handle != 0) && (dev != 0) && (ctx != 0);
	}

	bool OpenGLRendererWin::makeCurrent() const
	{
		return wglMakeCurrent(dev, ctx);
	}

	void OpenGLRendererWin::swapBuffers()
	{
		SwapBuffers(dev);
	}
}