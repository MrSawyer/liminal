#pragma once
#include <core/glew/glew.h>
//#include <core/macros/enum_flag_operations.h>
/*
* !!! TODO !!! Trzeba sie pozby� tej zale�no�ci, wydzieli� DEFINE_ENUM_FLAG_OPERATORS 
*/
#include <winnt.h>

namespace Faber::core
{
	enum class OpenGLBufferMask : unsigned int
	{
		COLOR_BUFFER = GL_COLOR_BUFFER_BIT,
		DEPTH_BUFFER = GL_DEPTH_BUFFER_BIT,
		STENCIL_BUFFER = GL_STENCIL_BUFFER_BIT
	};
}

DEFINE_ENUM_FLAG_OPERATORS(Faber::core::OpenGLBufferMask)