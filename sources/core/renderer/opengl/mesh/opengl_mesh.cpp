#include "opengl_mesh.h"
#include <core/glew/glew.h>

namespace Faber::core
{

    OpenGLMesh::OpenGLMesh(std::vector<engine::Vertex>&& vert, std::vector<unsigned int>&& ind, std::vector<engine::Texture>&& tex)
        :Mesh::Mesh(std::move(vert), std::move(ind), std::move(tex))
    {
        VAO = 0;
        VBO = 0;
        EBO = 0;
        setup();
    }

   OpenGLMesh::OpenGLMesh(const std::vector<engine::Vertex>& vert, const std::vector<unsigned int>& ind, std::vector<engine::Texture>& tex)
       :Mesh::Mesh(vert, ind, tex)
   {
       VAO = 0;
       VBO = 0;
       EBO = 0;
       setup();
   }

    void OpenGLMesh::setup()
    {
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glGenBuffers(1, &EBO);

        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);

        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(engine::Vertex), &vertices[0], GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int),
            &indices[0], GL_STATIC_DRAW);

        // vertex positions
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(engine::Vertex), (void*)0);
        // vertex normals
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(engine::Vertex), (void*)offsetof(engine::Vertex, normal));
        // vertex texture coords
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(engine::Vertex), (void*)offsetof(engine::Vertex, tex_coords));

        glBindVertexArray(0);
    }
}