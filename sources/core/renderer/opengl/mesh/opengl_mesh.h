#pragma once

#include <engine/graphics/mesh/mesh.h>

namespace Faber::core
{
	class OpenGLMesh : public virtual engine::Mesh
	{
	public: 
		OpenGLMesh(std::vector<engine::Vertex>&& vertices, std::vector<unsigned int>&& indices, std::vector<engine::Texture>&& textures);
		OpenGLMesh(const std::vector<engine::Vertex>& vertices, const std::vector<unsigned int>& indices, std::vector<engine::Texture>& textures);

		void setup() override;
	private:
		unsigned int VAO, VBO, EBO;
	};
}