#pragma once

#include <engine/graphics/shader/shader.h>

namespace Faber::core
{
	class OpenGLShader : public virtual engine::Shader
	{
		OpenGLShader();
		OpenGLShader(const OpenGLShader& other);
		~OpenGLShader();

		bool loadFromFiles(const char* vs_path, const char* fs_path) override;
		bool loadFromMemory(std::string vs_code, std::string fs_code)override;
		void terminateShader()override;
		void use()override;

		void send(int value, const char* target)override;
		void send(float value, const char* target)override;
		void send(glm::vec2 value, const char* target)override;
		void send(glm::vec3 value, const char* target)override;
		void send(glm::vec4 value, const char* target)override;
		void send(glm::mat3 value, const char* target)override;
		void send(glm::mat4 value, const char* target)override;
		//void send(Texture* texture, const char* target, int slot);
		//void send(PointLight* light, int slot);
		//void send(SpotLight* light, int slot);

	protected:
		bool includeFile(const char* path, std::string& target, size_t begin, size_t end)override;
		bool iterateIncludes(const char* directory, std::string& target)override;

	private:
		unsigned int id;
	};
}