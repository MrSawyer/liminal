#pragma once
#include <core/renderer/renderer.h>

namespace Faber::core {
	class OpenGLRenderer : public Renderer
	{
	public:
		OpenGLRenderer(Instance* instance_, unsigned int major_version_, unsigned int minor_version_) :
			Renderer::Renderer(std::move(instance_)), major_version(major_version_), minor_version(minor_version_)  {};
		virtual ~OpenGLRenderer() = default;

		std::string getInfo() const override;

		void enableDepthTest(bool enable) override;
		void enableStencilTest(bool enable) override;
		void setBackgroundColor(glm::vec4 color) override;
		void setDepthClearValue(double depth) override;
		void setStencilClearValue(int stencil) override;
		void setAsRenderTarget() override;
		void clearBuffers(BufferMask masks) override;

		bool constructModel();
		//TODO tu musze pomyslec czy shader nie powinien byc blueprintem do stworzenia shadera prawdziwego
		// wtedy bez klasy OpenGLShader
		bool constructShader();

	protected:
		void setOpenGLSettings();
		bool loadOpenGLExtensions();

		virtual bool getAccessToOpenGLExtensions() = 0;
		virtual bool createOpenGLContext(unsigned int major_version, unsigned int minor_version) = 0;
		virtual void deleteOpenGLContext() = 0;
	private:
		unsigned int major_version;
		unsigned int minor_version;
	};
}