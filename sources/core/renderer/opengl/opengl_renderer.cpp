#include "opengl_renderer.h"
#include <core/glew/glew.h>
#include <core/logger/logger.h>
#include "opengl_buffer_mask.h"

namespace Faber::core
{
	std::string OpenGLRenderer::getInfo() const
	{
		std::stringstream ss;
		ss << "[OpenGLRenderer{";
		ss << Renderer::getInfo();
		ss << GETINFO(major_version, minor_version);
		ss << "}]";
		return ss.str();
	}

	bool OpenGLRenderer::loadOpenGLExtensions()
	{
		if (glewInit() != GLEW_OK)
		{
			LOG_ERR("Loading OpenGL extensions failed.");
			return false;
		}
		LOG_INF("OpenGL extensions loaded.");
		return true;
	}

	void OpenGLRenderer::setOpenGLSettings()
	{
		//wglSwapIntervalEXT(0);

		setBackgroundColor(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
		setDepthClearValue(1.0);
		setStencilClearValue(0);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glEnable(GL_TEXTURE_2D);
		glEnable(GL_MULTISAMPLE);
	}

	void OpenGLRenderer::setAsRenderTarget()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void OpenGLRenderer::setBackgroundColor(glm::vec4 color)
	{
		glClearColor(color.r, color.g, color.b, color.a);
	}

	void OpenGLRenderer::setDepthClearValue(double depth)
	{
		glClearDepth(depth);
	}

	void OpenGLRenderer::setStencilClearValue(int stencil)
	{
		glClearStencil(stencil);
	}

	void OpenGLRenderer::enableDepthTest(bool enable)
	{
		if (enable) glEnable(GL_DEPTH_TEST);
		else glDisable(GL_DEPTH_TEST);
	}

	void OpenGLRenderer::enableStencilTest(bool enable)
	{
		if (enable) glEnable(GL_STENCIL_TEST);
		else glDisable(GL_STENCIL_TEST);
	}

	void OpenGLRenderer::clearBuffers(BufferMask masks)
	{
		OpenGLBufferMask opengl_mask = OpenGLBufferMask::COLOR_BUFFER;
		switch (masks)
		{
			case BufferMask::COLOR_BUFFER: opengl_mask = OpenGLBufferMask::COLOR_BUFFER; break;
			case BufferMask::DEPTH_BUFFER: opengl_mask = OpenGLBufferMask::DEPTH_BUFFER; break;
			case BufferMask::STENCIL_BUFFER: opengl_mask = OpenGLBufferMask::STENCIL_BUFFER; break;
		}
		glClear(static_cast<GLbitfield>(opengl_mask));
	}
}