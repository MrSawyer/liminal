#pragma once
#include <engine/assets/asset.h>

namespace Faber::engine
{
	class ShaderData : public virtual Asset
	{
	public:
		virtual ~ShaderData() = 0;
		std::string getInfo() const override;

		enum class ShaderType
		{
			Vertex,
			Tessellation,
			Evaluation,
			Geometry,
			Fragment,
			Compute
		};
	protected:

	private:

	};
}