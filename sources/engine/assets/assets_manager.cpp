#include "assets_manager.h"
#include <core/logger/logger.h>
#include <filesystem>

#include <core/assimp/Importer.hpp>
//#include <core/assimp/scene.h>
#include <core/assimp/postprocess.h>

namespace Faber::engine {
	std::string AssetsManager::getInfo() const
	{
		std::stringstream ss;
		ss << "[AssetsManager{";
		ss << GETINFO(loaded_assets.size());
		ss << "}]";
		return ss.str();
	}


	void AssetsManager::processNode(aiNode* node, const aiScene* scene)
	{
		// process all the node's meshes (if any)
		for (unsigned int i = 0; i < node->mNumMeshes; i++)
		{
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
			//meshes.push_back(processMesh(mesh, scene));
		}
		// then do the same for each of its children
		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			processNode(node->mChildren[i], scene);
		}
	}


	std::optional<MeshData*> AssetsManager::loadMesh(const std::string& path, std::string name)
	{
		if (!std::filesystem::exists(path))
		{
			LOG_ERR(path << " does not exists.");
			return std::nullopt;
		}

		name = name.empty() ? path : name;

		if (name.empty())
		{
			LOG_ERR("Name cannot be empty.");
			return std::nullopt;
		}

		if (loaded_assets.contains(std::hash<std::string>{}(path)))
		{
			LOG_ERR(name << " already loaded.");
			return std::nullopt;
		}

		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
		
		if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
		{
			LOG_ERR("ERROR::ASSIMP::" << importer.GetErrorString());
			return  std::nullopt;
		}

		LoadedAsset new_asset;
		new_asset.directory = path.substr(0, path.find_last_of('/'));

		aiNode* root_node = scene->mRootNode;

		if (root_node->mNumMeshes == 0)
		{
			LOG_ERR("There are no meshes data in " << path);
			return  std::nullopt;
		}

		if (root_node->mNumMeshes > 1)LOG_WAR("There are multiple meshes data in " << path << " loading first of them.");

		aiMesh* mesh = scene->mMeshes[root_node->mMeshes[0]];

		new_asset.asset = processMesh(mesh, scene);


		//auto pair_it = loaded_assets.insert(std::make_pair(new_asset.directory, std::move(new_asset)));
		

		return  std::nullopt;
	}

	std::unique_ptr<MeshData> AssetsManager::processMesh(aiMesh* mesh, const aiScene* scene)
	{
		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;
		std::vector<Texture> textures;

		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			Vertex vertex;
			// process vertex positions, normals and texture coordinates
			glm::vec3 vector;
			vector.x = mesh->mVertices[i].x;
			vector.y = mesh->mVertices[i].y;
			vector.z = mesh->mVertices[i].z;
			vertex.position = vector;

			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.normal = vector;

			if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
			{
				glm::vec2 vec;
				vec.x = mesh->mTextureCoords[0][i].x;
				vec.y = mesh->mTextureCoords[0][i].y;
				vertex.tex_coords = vec;
			}
			else vertex.tex_coords = glm::vec2(0.0f, 0.0f);

			vertices.push_back(vertex);
		}

		// process indices
		for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];
			for (unsigned int j = 0; j < face.mNumIndices; j++)
				indices.push_back(face.mIndices[j]);
		}

		// process material
		/*
		if (mesh->mMaterialIndex >= 0)
		{
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

			std::vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
			textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
			
			std::vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
			textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		}
		*/
		return nullptr;
		//return Mesh(vertices, indices, textures);

	}
	std::optional<ModelData*> AssetsManager::loadModel(const std::string& path, std::string name)
	{
		return std::nullopt;
	}


	/*
	std::vector<Texture> AssetsManager::loadMaterialTextures(aiMaterial* material, aiTextureType type, std::string typeName)
	{

		std::vector<Texture> textures;
		for (unsigned int i = 0; i < material->GetTextureCount(type); i++)
		{
			aiString str;
			material->GetTexture(type, i, &str);
			//Texture texture;
			//texture.id = TextureFromFile(str.C_Str(), directory);
			//texture.type = typeName;
			//texture.path = str;
			//textures.push_back(texture);
		}
		return textures;

	}
			*/
}