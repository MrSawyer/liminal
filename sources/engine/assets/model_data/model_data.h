#pragma once
#include <engine/assets/asset.h>
#include <engine/assets/mesh_data/mesh_data.h>
#include <list>

namespace Faber::engine
{
	class ModelData : public virtual Asset
	{
	public:
		virtual ~ModelData() = 0;
		std::string getInfo() const override;

	protected:

	private:
		std::list<MeshData*> meshes;
	};
}