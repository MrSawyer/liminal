#pragma once
#include <engine/engine_object/engine_object.h>
#include <engine/assets/asset.h>
#include <core/renderer/renderer.h>

#include <engine/assets/mesh_data/mesh_data.h>
#include <engine/assets/model_data/model_data.h>
#include <engine/assets/shader_data/shader_data.h>

#include <string>
#include <core/assimp/scene.h>
#include <unordered_map>
#include <memory>

/*
* TODO na razie funkcje �adujace przyjmuj� renderer jako argument ale trzeba to zmieni�
*/

namespace Faber::engine {

	class AssetsManager : public virtual EngineObject
	{
	public:
		virtual ~AssetsManager() = default;

		std::string getInfo() const override;


		std::optional<ModelData*> loadModel(const std::string& path, std::string name = "");
		//std::optional<Model *> loadModel(std::string&& path, core::Renderer* renderer);

		//std::optional<Shader*> loadShader(const std::string& path);
		//std::optional<Shader*> loadShader(std::string&& path);

	private:
		std::unique_ptr<MeshData> processMesh(aiMesh* mesh, const aiScene* scene);
		//std::vector<Texture> loadMaterialTextures(aiMaterial* material, aiTextureType type, std::string typeName);

		void processNode(aiNode* node, const aiScene* scene);

		struct LoadedAsset
		{
			std::unique_ptr<Asset> asset;
			std::string directory;
			std::string name;
			std::size_t hash; // hash -> hash(directory+name) .... std::hash<std::string>
		};

		std::unordered_map<std::size_t, LoadedAsset> loaded_assets;
	};
}