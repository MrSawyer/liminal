#pragma once
#include <engine/engine_object/engine_object.h>

namespace Faber::engine {
	class Asset : public virtual EngineObject
	{
	public:
		virtual ~Asset() = 0;

		std::string getInfo() const override;

	};

}
