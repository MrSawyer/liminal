#pragma once
#include <engine/assets/asset.h>
#include <engine/graphics/types/vertex.h>
#include <engine/graphics/texture/texture.h>

#include <vector>

namespace Faber::engine
{
	class MeshData : public virtual Asset
	{
	public:
		virtual ~MeshData() = 0;
		std::string getInfo() const override;

	protected:

	private:
		std::vector<Vertex>       vertices;
		std::vector<unsigned int> indices;
		std::vector<Texture>      textures;
	};
}