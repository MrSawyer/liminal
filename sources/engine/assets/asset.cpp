#include "asset.h"
#include <core/logger/logger.h>

namespace Faber::engine {
	Asset::~Asset()
	{

	}

	std::string Asset::getInfo() const
	{
		std::stringstream ss;
		ss << "[Asset{";
		//ss << GETINFO(vertices.size(), indices.size(), textures.size());
		ss << "}]";
		return ss.str();
	}

}