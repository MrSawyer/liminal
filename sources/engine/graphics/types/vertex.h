#pragma once
#include <core/glm/glm/glm.hpp>

namespace Faber::engine {
    struct Vertex 
    {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec2 tex_coords;
    };
}