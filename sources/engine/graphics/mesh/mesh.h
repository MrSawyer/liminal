#pragma once

#include <engine/actor/actor.h>
#include <engine/assets/mesh_data/mesh_data.h>

#include <vector>

namespace Faber::engine
{
	class Mesh : public virtual Actor
	{
    public:
        explicit Mesh(MeshData * data);

        virtual ~Mesh() = 0;

        virtual void draw() = 0;

        std::string getInfo() const override;

    protected:
        virtual void setup() = 0;
        MeshData* data;
        bool good;
	};
}