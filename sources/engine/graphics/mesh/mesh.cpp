#include "mesh.h"
#include <core/logger/logger.h>

namespace Faber::engine
{
    std::string Mesh::getInfo() const
    {
        std::stringstream ss;
        ss << "[Mesh{";
        ss << (data != nullptr ? data->getInfo() : "");
        ss << GETINFO(good);
        ss << "}]";
        return ss.str();
    }

    Mesh::Mesh(MeshData* mesh_data)
    {
        if (data == nullptr)
        {
            LOG_ERR("Mesh data empty.");
        }
        else
        {
            data = std::move(mesh_data);
        }
    }

}