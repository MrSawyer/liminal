#pragma once

#include <engine/actor/actor.h>
#include <engine/assets/model_data/model_data.h>

namespace Faber::engine
{
	class Model : public virtual Actor
	{
	public:
		std::string getInfo() const override;
	protected:

	private:
		ModelData* data;
		bool good;
	};
}