#include "model.h"
#include <core/logger/logger.h>

namespace Faber::engine
{
    std::string Model::getInfo() const
    {
        std::stringstream ss;
        ss << "[Model{";
        ss << (data != nullptr ? data->getInfo() : "");
        ss << GETINFO(good);
        ss << "}]";
        return ss.str();
    }

}