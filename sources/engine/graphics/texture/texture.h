#pragma once
#include <engine/engine_object/engine_object.h>
#include <string>

namespace Faber::engine {
    class Texture : public virtual EngineObject
    {
    public:

    private:
        unsigned int id;
        std::string type;
    };
}