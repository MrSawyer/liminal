#pragma once
#include <engine/engine_object/engine_object.h>

#include <core/glm/glm/glm.hpp>

namespace Faber::engine
{
	class Shader : public virtual EngineObject
	{
	public:
		Shader() = default;
		Shader(const Shader& other) = delete;
		virtual ~Shader() = 0;

		std::string getInfo() const override;

		virtual bool loadFromFiles(const char* vs_path, const char* fs_path) = 0;
		virtual bool loadFromMemory(std::string vs_code, std::string fs_code) = 0;
		virtual void terminateShader() = 0;
		virtual void use() = 0;

		virtual void send(int value, const char* target) = 0;
		virtual void send(float value, const char* target) = 0;
		virtual void send(glm::vec2 value, const char* target) = 0;
		virtual void send(glm::vec3 value, const char* target) = 0;
		virtual void send(glm::vec4 value, const char* target) = 0;
		virtual void send(glm::mat3 value, const char* target) = 0;
		virtual void send(glm::mat4 value, const char* target) = 0;
		//void send(Texture* texture, const char* target, int slot);
		//void send(PointLight* light, int slot);
		//void send(SpotLight* light, int slot);
		
		// TODO tu musz� pomy�le� jak to rozwi�za� bo de facto shader powinien by� �adowany z asset managera
		//virtual bool make() = 0;
		//virtual bool compile(std::string shader_code, ShaderType type) = 0;
		virtual void setup() = 0;

	protected:
		virtual bool includeFile(const char* path, std::string& target, size_t begin, size_t end) = 0;
		virtual bool iterateIncludes(const char* directory, std::string& target) = 0;
	};

}