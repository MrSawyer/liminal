#pragma once
#include <engine/actor/actor.h>

namespace Faber::engine
{
	class Camera : public virtual Actor
	{
	public:
		virtual ~Camera() = 0;
		std::string getInfo() const override;

	private:
		glm::vec3 target;
		glm::vec3 up;
	};
}