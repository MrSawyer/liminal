#pragma once
#include <engine/engine_object/engine_object.h>
#include <engine/transformable/transformable.h>

namespace Faber::engine
{
	class Actor : public Transformable, public virtual EngineObject
	{
	public:
		virtual ~Actor() = 0;

		virtual std::string getInfo() const override;

	private:
		Actor* parent;
	};
}