#include "actor.h"
#include <core/logger/logger.h>

namespace Faber::engine {

	std::string Actor::getInfo() const
	{
		std::stringstream ss;
		ss << "[Actor{";
		ss << "}]";
		return ss.str();
	}
}