#pragma once
#include <string>

namespace Faber::engine
{
	class EngineObject
	{
	public:
		virtual std::string getInfo() const = 0;/**< Each class in the program should have a getInfo function 
												that returns a string describing the state of the class object
												*/
		virtual ~EngineObject() {}
	};
}
