#pragma once

namespace Faber::engine
{
	class Engine
	{
	public:
		void parseArguments();
		void init();
		void start();
		void close();
	private:
	};
}