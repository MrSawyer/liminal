#include "engine.h"
#include <core/logger/logger.h>

namespace Faber::engine
{
	void Engine::parseArguments()
	{
		LOG_INF("Engine parseArguments");

	}

	void Engine::init()
	{
		LOG_INF("Engine init");

	}

	void Engine::start()
	{
		LOG_INF("Engine start");

	}

	void Engine::close()
	{
		LOG_INF("Engine close");

	}
}