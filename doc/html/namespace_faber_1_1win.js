var namespace_faber_1_1win =
[
    [ "InstanceWin", "class_faber_1_1win_1_1_instance_win.html", "class_faber_1_1win_1_1_instance_win" ],
    [ "OpengGLRendererWin", "class_faber_1_1win_1_1_openg_g_l_renderer_win.html", "class_faber_1_1win_1_1_openg_g_l_renderer_win" ],
    [ "SystemFactoryWin", "class_faber_1_1win_1_1_system_factory_win.html", "class_faber_1_1win_1_1_system_factory_win" ],
    [ "WindowWin", "class_faber_1_1win_1_1_window_win.html", "class_faber_1_1win_1_1_window_win" ],
    [ "OpenGLBufferMask", "namespace_faber_1_1win.html#a4e9edd746d25e3afb36b95bcab674eba", [
      [ "COLOR_BUFFER", "namespace_faber_1_1win.html#a4e9edd746d25e3afb36b95bcab674ebaa6fca62c93094a122f0905a0536a1680b", null ],
      [ "DEPTH_BUFFER", "namespace_faber_1_1win.html#a4e9edd746d25e3afb36b95bcab674ebaaf354699041a23b899f3431bacebb6db6", null ],
      [ "STENCIL_BUFFER", "namespace_faber_1_1win.html#a4e9edd746d25e3afb36b95bcab674ebaa41baf0282a4cf8168d42afc8420e7801", null ]
    ] ]
];