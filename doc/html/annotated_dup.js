var annotated_dup =
[
    [ "Faber", "namespace_faber.html", [
      [ "core", "namespace_faber_1_1core.html", [
        [ "win", "namespace_faber_1_1core_1_1win.html", [
          [ "InstanceWin", "class_faber_1_1core_1_1win_1_1_instance_win.html", "class_faber_1_1core_1_1win_1_1_instance_win" ],
          [ "OpengGLRendererWin", "class_faber_1_1core_1_1win_1_1_openg_g_l_renderer_win.html", "class_faber_1_1core_1_1win_1_1_openg_g_l_renderer_win" ],
          [ "SystemFactoryWin", "class_faber_1_1core_1_1win_1_1_system_factory_win.html", "class_faber_1_1core_1_1win_1_1_system_factory_win" ],
          [ "WindowWin", "class_faber_1_1core_1_1win_1_1_window_win.html", "class_faber_1_1core_1_1win_1_1_window_win" ]
        ] ],
        [ "BasicTimer", "class_faber_1_1core_1_1_basic_timer.html", "class_faber_1_1core_1_1_basic_timer" ],
        [ "EngineLoop", "class_faber_1_1core_1_1_engine_loop.html", "class_faber_1_1core_1_1_engine_loop" ],
        [ "Instance", "class_faber_1_1core_1_1_instance.html", "class_faber_1_1core_1_1_instance" ],
        [ "Logger", "class_faber_1_1core_1_1_logger.html", "class_faber_1_1core_1_1_logger" ],
        [ "Observer", "class_faber_1_1core_1_1_observer.html", "class_faber_1_1core_1_1_observer" ],
        [ "ObserverMessage", "struct_faber_1_1core_1_1_observer_message.html", "struct_faber_1_1core_1_1_observer_message" ],
        [ "ObserverSubject", "class_faber_1_1core_1_1_observer_subject.html", "class_faber_1_1core_1_1_observer_subject" ],
        [ "Renderer", "class_faber_1_1core_1_1_renderer.html", "class_faber_1_1core_1_1_renderer" ],
        [ "SystemFactory", "class_faber_1_1core_1_1_system_factory.html", "class_faber_1_1core_1_1_system_factory" ],
        [ "Window", "class_faber_1_1core_1_1_window.html", "class_faber_1_1core_1_1_window" ]
      ] ],
      [ "engine", "namespace_faber_1_1engine.html", [
        [ "Camera", "class_faber_1_1engine_1_1_camera.html", null ],
        [ "Engine", "class_faber_1_1engine_1_1_engine.html", "class_faber_1_1engine_1_1_engine" ],
        [ "EngineObject", "class_faber_1_1engine_1_1_engine_object.html", "class_faber_1_1engine_1_1_engine_object" ],
        [ "Mesh", "class_faber_1_1engine_1_1_mesh.html", "class_faber_1_1engine_1_1_mesh" ],
        [ "Model", "class_faber_1_1engine_1_1_model.html", null ],
        [ "Shader", "class_faber_1_1engine_1_1_shader.html", "class_faber_1_1engine_1_1_shader" ],
        [ "Texture", "class_faber_1_1engine_1_1_texture.html", null ],
        [ "Transformable", "class_faber_1_1engine_1_1_transformable.html", "class_faber_1_1engine_1_1_transformable" ],
        [ "Vertex", "struct_faber_1_1engine_1_1_vertex.html", "struct_faber_1_1engine_1_1_vertex" ]
      ] ],
      [ "input", "namespace_faber_1_1input.html", [
        [ "InputController", "class_faber_1_1input_1_1_input_controller.html", "class_faber_1_1input_1_1_input_controller" ],
        [ "KeyboardController", "class_faber_1_1input_1_1_keyboard_controller.html", null ],
        [ "MouseController", "class_faber_1_1input_1_1_mouse_controller.html", null ]
      ] ]
    ] ]
];