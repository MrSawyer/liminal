var dir_8d74c0b8c07076568206226e61817697 =
[
    [ "observer.cpp", "observer_8cpp.html", "observer_8cpp" ],
    [ "observer.h", "observer_8h.html", [
      [ "Faber::core::ObserverMessage", "struct_faber_1_1core_1_1_observer_message.html", "struct_faber_1_1core_1_1_observer_message" ],
      [ "Faber::core::Observer", "class_faber_1_1core_1_1_observer.html", "class_faber_1_1core_1_1_observer" ],
      [ "Faber::core::ObserverSubject", "class_faber_1_1core_1_1_observer_subject.html", "class_faber_1_1core_1_1_observer_subject" ]
    ] ]
];