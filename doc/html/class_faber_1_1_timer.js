var class_faber_1_1_timer =
[
    [ "Timer", "class_faber_1_1_timer.html#a34d7ce6cef3f1bae3934c3850a0c86fe", null ],
    [ "getMiliseconds", "class_faber_1_1_timer.html#ad2d17c809251d6e1a163a260003b4782", null ],
    [ "reset", "class_faber_1_1_timer.html#a404075fb0ee87962b69e6a7a545e5dae", null ],
    [ "start", "class_faber_1_1_timer.html#a97278f09bff74e1a59400291553b823f", null ],
    [ "stop", "class_faber_1_1_timer.html#aead038528056a5252deacd4e700f30b6", null ]
];