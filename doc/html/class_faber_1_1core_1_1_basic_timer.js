var class_faber_1_1core_1_1_basic_timer =
[
    [ "BasicTimer", "class_faber_1_1core_1_1_basic_timer.html#adb9ac368723968f38b946fe091235341", null ],
    [ "getMiliseconds", "class_faber_1_1core_1_1_basic_timer.html#ad7fe5b06f18f19012ae9a98346e4f64a", null ],
    [ "reset", "class_faber_1_1core_1_1_basic_timer.html#a601d42ae0cc843b305df9eaea2362bcc", null ],
    [ "start", "class_faber_1_1core_1_1_basic_timer.html#aff1b6ec5082ef5f7c72e225333097c8c", null ],
    [ "stop", "class_faber_1_1core_1_1_basic_timer.html#a73490a722873d04cf2c681310a25594c", null ]
];